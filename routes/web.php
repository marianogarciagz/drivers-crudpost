<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConductorController;
use App\Http\Controllers\VehicleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/conductors', [ConductorController::class, 'index']);
Route::get('/vehicles', [VehicleController::class, 'index'])->name('vehicles.index');





Route::get('/conductors/create', [ConductorController::class, 'create'])->name('conductors.create');
Route::post('/conductors', [ConductorController::class, 'store'])->name('conductors.store');


Route::resource('conductors', ConductorController::class);

Route::get('/conductors/{id}/edit', 'ConductorController@edit')->name('conductors.edit');

Route::get('/conductors/{id}', 'ConductorController@show')->name('conductors.show');

Route::get('/conductors/search', 'ConductorController@search')->name('conductors.search');

Route::get('/vehicles/create', [VehicleController::class, 'create'])->name('vehicles.create');

Route::post('/vehicles', [VehicleController::class, 'store'])->name('vehicles.store');

Route::delete('/vehicles/{id}', [VehicleController::class, 'destroy'])->name('vehicles.destroy');

Route::get('/vehicles/{id}/edit', [VehicleController::class, 'edit'])->name('vehicles.edit');

Route::put('/vehicles/{id}', [VehicleController::class, 'update'])->name('vehicles.update');

Route::get('/vehicles/{id}', [VehicleController::class, 'show'])->name('vehicles.show');

Route::get('/vehicles/search', [VehicleController::class, 'search'])->name('vehicles.search');


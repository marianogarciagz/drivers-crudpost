<!DOCTYPE html>
<html>
<head>
    <title>Detalles del Vehículo</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold mb-4">Detalles del Vehículo</h1>

    <div class="bg-white p-6 rounded-lg shadow-lg">
        <p class="mb-2"><strong>ID:</strong> {{ $vehicle->id }}</p>
        <p class="mb-2"><strong>Modelo:</strong> {{ $vehicle->modelo }}</p>
        <p class="mb-2"><strong>Placa:</strong> {{ $vehicle->placa }}</p>
        <p class="mb-2"><strong>Conductor:</strong> {{ $vehicle->conductor->nombre }}</p>
        <p class="mb-2"><strong>Reseña:</strong> {{ $vehicle->resenia }}</p>

        <a href="{{ route('vehicles.index') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline inline-block mt-4">Volver</a>
    </div>
</body>
</html>

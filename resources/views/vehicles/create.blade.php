<!DOCTYPE html>
<html>
<head>
    <title>Crear Vehículo</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold mb-4">Crear Vehículo</h1>

    <div class="bg-white p-6 rounded-lg shadow-lg">
        <form method="POST" action="{{ route('vehicles.store') }}">
            @csrf

            <div class="mb-4">
                <label for="modelo" class="block text-gray-700 font-semibold">Modelo:</label>
                <input type="text" name="modelo" id="modelo" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
            </div>

            <div class="mb-4">
                <label for="placa" class="block text-gray-700 font-semibold">Placa:</label>
                <input type="text" name="placa" id="placa" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
            </div>

            <div class="mb-4">
                <label for="conductor_id" class="block text-gray-700 font-semibold">Conductor:</label>
                <select name="conductor_id" id="conductor_id" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                    @foreach ($conductors as $conductor)
                        <option value="{{ $conductor->id }}">{{ $conductor->nombre }} {{ $conductor->apellido }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-4">
                <label for="resenia" class="block text-gray-700 font-semibold">Reseña:</label>
                <textarea name="resenia" id="resenia" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"></textarea>
            </div>

            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Crear Vehículo</button>
        </form>

        <a href="{{ route('vehicles.index') }}" class="bg-gray-400 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-4 inline-block">Volver</a>
    </div>
</body>
</html>

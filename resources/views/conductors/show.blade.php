<!DOCTYPE html>
<html>
<head>
    <title>Detalles del Conductor</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold mb-4">Detalles del Conductor</h1>

    <div class="bg-white p-6 rounded-lg shadow-lg">
        <p class="mb-4"><strong>ID:</strong> {{ $conductor->id }}</p>
        <p class="mb-4"><strong>Nombre:</strong> {{ $conductor->nombre }}</p>
        <p class="mb-4"><strong>Teléfono:</strong> {{ $conductor->telefono }}</p>
        <p class="mb-4"><strong>Correo:</strong> {{ $conductor->correo }}</p>
      

        <a href="{{ route('conductors.index') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline inline-block">Volver</a>
    </div>
</body>
</html>

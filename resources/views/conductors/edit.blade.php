<!DOCTYPE html>
<html>
<head>
    <title>Editar Conductor</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold mb-4">Editar Conductor</h1>

    <div class="bg-white p-6 rounded-lg shadow-lg">
        <form action="{{ route('conductors.update', $conductor->id) }}" method="POST">
            @csrf
            @method('PUT')
            <!-- Campos del formulario -->
            <div class="mb-4">
                <label for="nombre" class="block text-gray-700 font-semibold">Nombre:</label>
                <input type="text" name="nombre" id="nombre" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ $conductor->nombre }}" required>
            </div>

            <div class="mb-4">
                <label for="telefono" class="block text-gray-700 font-semibold">Teléfono:</label>
                <input type="text" name="telefono" id="telefono" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ $conductor->telefono }}" required>
            </div>

            <div class="mb-6">
                <label for="correo" class="block text-gray-700 font-semibold">Correo:</label>
                <input type="text" name="correo" id="correo" class="border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" value="{{ $conductor->correo }}" required>
            </div>


            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Guardar Cambios</button>
        </form>
    </div>
</body>
</html>

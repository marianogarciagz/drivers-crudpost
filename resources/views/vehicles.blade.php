<!DOCTYPE html>
<html>
<head>
    <title>Lista de Vehículos</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold mb-4">Lista de Vehículos</h1>
    
    <div class="flex items-center mb-4">
        <a href="{{ route('vehicles.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2">Crear</a>
        
        <form action="{{ route('vehicles.search') }}" method="GET" class="flex-1">
            @csrf
            <div class="flex items-center border rounded py-2 px-3">

            </div>
        </form>
        
        <a href="{{ route('conductors.index') }}" class="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 ml-2">Volver</a>
    </div>

    <table class="w-full border-collapse border border-gray-400">
        <thead>
            <tr>
                <th class="border border-gray-400 px-4 py-2">ID</th>
                <th class="border border-gray-400 px-4 py-2">Modelo</th>
                <th class="border border-gray-400 px-4 py-2">Placa</th>
                <th class="border border-gray-400 px-4 py-2">Conductor</th>
                <th class="border border-gray-400 px-4 py-2">Reseña</th>
                <th class="border border-gray-400 px-4 py-2">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($vehicles as $vehicle)
                <tr>
                    <td class="border border-gray-400 px-4 py-2">{{ $vehicle->id }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $vehicle->modelo }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $vehicle->placa }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $vehicle->conductor->nombre }} {{ $vehicle->conductor->apellido }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $vehicle->resenia }}</td>
                    <td class="border border-gray-400 px-4 py-2">
                        <a href="{{ route('vehicles.show', $vehicle->id) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded inline-block">Detalles</a>
                        <a href="{{ route('vehicles.edit', $vehicle->id) }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded inline-block ml-2">Editar</a>
                        <form action="{{ route('vehicles.destroy', $vehicle->id) }}" method="POST" class="inline-block ml-2">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded" onclick="return confirm('¿Estás seguro de eliminar este vehículo?')">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>Lista de Conductores</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100 p-8">
    <h1 class="text-3xl font-bold">Lista de Conductores</h1>

    <div class="flex flex-col md:flex-row items-center justify-between mt-4 mb-8">
        <a href="{{ route('conductors.create') }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-4 md:mb-0">Crear</a>
        
        <div class="flex flex-col md:flex-row items-center mb-4">
            <input type="text" name="search" id="search" class="border rounded py-2 px-3 mb-2 md:mb-0 md:mr-2" placeholder="Buscar">
          
        </div>
    
        <a href="{{ route('vehicles.index') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded inline-block">Ver Vehículos</a>
    </div>
    

    <table class="w-full border-collapse border border-gray-400 rounded">
        <thead>
            <tr>
                <th class="border border-gray-400 px-4 py-2">ID</th>
                <th class="border border-gray-400 px-4 py-2">Nombre</th>
                <th class="border border-gray-400 px-4 py-2">Teléfono</th>
                <th class="border border-gray-400 px-4 py-2">Correo</th>
                <th class="border border-gray-400 px-4 py-2">Fecha de Creación</th>
                <th class="border border-gray-400 px-4 py-2">Última Actualización</th>
                <th class="border border-gray-400 px-4 py-2">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($conductors as $conductor)
                <tr>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->id }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->nombre }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->telefono }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->correo }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->created_at }}</td>
                    <td class="border border-gray-400 px-4 py-2">{{ $conductor->updated_at }}</td>
                    <td class="border border-gray-400 px-4 py-2">
                        <a href="{{ route('conductors.show', $conductor->id) }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded inline-block">Detalles</a>
                        <a href="{{ route('conductors.edit', $conductor->id) }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded inline-block ml-2">Editar</a>
                        <form action="{{ route('conductors.destroy', $conductor->id) }}" method="POST" class="inline-block ml-2">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded" onclick="return confirm('¿Estás seguro de eliminar a este conductor?')">Eliminar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function () {
        // Función para realizar la búsqueda
        function searchVehicles() {
            var searchTerm = $('#search').val().toLowerCase();

            $('tbody tr').each(function () {
                var text = $(this).text().toLowerCase();
                if (text.indexOf(searchTerm) === -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        }

        // Llama a la función de búsqueda cuando se hace clic en el botón
        $('#searchButton').click(function () {
            searchVehicles();
        });

        // Llama a la función de búsqueda cuando se cambia el valor en el campo de entrada de texto
        $('#search').on('input', function () {
            searchVehicles();
        });
    });
</script>

</body>
</html>


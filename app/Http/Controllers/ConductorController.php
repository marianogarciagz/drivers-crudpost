<?php

namespace App\Http\Controllers;

use App\Models\conductor;
use Illuminate\Http\Request;

class ConductorController extends Controller
{
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $conductors = Conductor::all(); // Obtiene todos los conductores de la tabla "conductors".
    return view('conductors', compact('conductors'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('conductors.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255',
            'telefono' => 'required|string|max:255',
            'correo' => 'required|email',

            // Agrega otras reglas de validación según tus campos
        ]);
    
        // Crea un nuevo objeto Conductor con los datos del formulario
        $conductor = new Conductor();
        $conductor->nombre = $request->input('nombre');
        $conductor->telefono = $request->input('telefono');
        $conductor->correo = $request->input('correo');

        // Asigna otros campos según sea necesario
    
        // Guarda el nuevo conductor en la base de datos
        $conductor->save();
    
        // Redirige a la página de listado de conductores u otra acción
        return redirect('/conductors')->with('success', 'Conductor creado exitosamente.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $conductor = Conductor::findOrFail($id);
    return view('conductors.show', compact('conductor'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $conductor = Conductor::findOrFail($id);
        return view('conductors.edit', compact('conductor'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $conductor = Conductor::findOrFail($id);
    $conductor->update($request->all());
    return redirect('/conductors')->with('success', 'Conductor eliminado exitosamente.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $conductor = Conductor::findOrFail($id);
    $conductor->delete();
    return redirect('/conductors')->with('success', 'Conductor eliminado exitosamente.');

    }


    public function search(Request $request)
{
    $searchTerm = $request->input('search');
    $conductors = Conductor::where('nombre', 'LIKE', "%$searchTerm%")->get();

    if ($request->ajax()) {
        return view('conductors.partial.list', compact('conductors'));
    }

    // Si no es una solicitud AJAX, puedes redirigir o mostrar una vista completa según tus necesidades.
}


}
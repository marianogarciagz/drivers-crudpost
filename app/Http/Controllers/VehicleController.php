<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Conductor;

use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $vehicles = Vehicle::with('conductor')->get();
        return view('vehicles', compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Aquí puedes cargar los conductores disponibles para mostrar en el select
    $conductors = Conductor::all();

    return view('vehicles.create', compact('conductors'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
      // Validación de datos
    $request->validate([
        'modelo' => 'required|string|max:255',
        'placa' => 'required|string|max:10',
        'conductor_id' => 'required|exists:conductors,id', // Valida que el conductor exista en la tabla de conductores
        'resenia' => 'nullable|string',
    ]);

    // Crear una nueva instancia de Vehicle con los datos del formulario
    $vehicle = new Vehicle([
        'modelo' => $request->input('modelo'),
        'placa' => $request->input('placa'),
        'conductor_id' => $request->input('conductor_id'),
        'resenia' => $request->input('resenia'),
    ]);

    // Guardar el vehículo en la base de datos
    $vehicle->save();

    // Redirigir a la página de la lista de vehículos con un mensaje de éxito
    return redirect()->route('vehicles.index')->with('success', 'Vehículo creado exitosamente');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        // Encuentra el vehículo por su ID
    $vehicle = Vehicle::findOrFail($id);

    return view('vehicles.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        // Encuentra el vehículo por su ID
    $vehicle = Vehicle::findOrFail($id);

    // Recupera la lista de conductores disponibles para mostrar en el select
    $conductors = Conductor::all();

    return view('vehicles.edit', compact('vehicle', 'conductors'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
         // Validación de datos
    $request->validate([
        'modelo' => 'required|string|max:255',
        'placa' => 'required|string|max:10',
        'conductor_id' => 'required|exists:conductors,id', // Valida que el conductor exista en la tabla de conductores
        'resenia' => 'nullable|string',
    ]);

    // Encuentra el vehículo por su ID
    $vehicle = Vehicle::findOrFail($id);

    // Actualiza los campos del vehículo con los datos del formulario
    $vehicle->modelo = $request->input('modelo');
    $vehicle->placa = $request->input('placa');
    $vehicle->conductor_id = $request->input('conductor_id');
    $vehicle->resenia = $request->input('resenia');

    // Guarda los cambios en la base de datos
    $vehicle->save();

    // Redirige de vuelta a la lista de vehículos con un mensaje de éxito
    return redirect()->route('vehicles.index')->with('success', 'Vehículo actualizado exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // Encuentra el vehículo por su ID y elimínalo
    $vehicle = Vehicle::findOrFail($id);
    $vehicle->delete();

    // Redirige de vuelta a la lista de vehículos con un mensaje de éxito
    return redirect()->route('vehicles.index')->with('success', 'Vehículo eliminado exitosamente');
    }

    public function search(Request $request)
{
    // Obtén el término de búsqueda ingresado por el usuario
    $searchTerm = $request->input('search');

    // Realiza la búsqueda en la base de datos
    $vehicles = Vehicle::select('vehicles.*')
        ->join('conductors', 'vehicles.conductor_id', '=', 'conductors.id')
        ->where('conductors.nombre', 'LIKE', '%' . $searchTerm . '%')
        ->get();
    // Retorna la vista con los resultados de la búsqueda
    return view('vehicles.index', compact('vehicles'));
}
}

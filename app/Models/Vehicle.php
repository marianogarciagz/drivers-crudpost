<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = ['modelo', 'placa', 'conductor_id','resenia'];

    public function conductor()
    {
        return $this->belongsTo(Conductor::class);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
    $table->string('modelo');
    $table->string('placa');
    $table->unsignedBigInteger('conductor_id'); // Llave foránea
    $table->text('resenia'); // Agregar la columna "resenia"
    $table->timestamps();

    $table->foreign('conductor_id')->references('id')->on('conductors');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehicles');
    }
};
